FROM golang:1.14 as builder
ARG APP_VERSION=""

WORKDIR /app
COPY . /app

RUN GO111MODULE=auto CGO_ENABLED=0 GOOS=linux GOPROXY=https://proxy.golang.org go build -ldflags="-w -s -X main.AppVersion=${APP_VERSION}" -o app cmd/app/main.go
RUN echo $APP_VERSION

FROM alpine:latest
RUN apk --no-cache add ca-certificates mailcap && addgroup -S app && adduser -S app -G app
WORKDIR /app
COPY --from=builder /app/app .
EXPOSE 8080
ENTRYPOINT ["./app"]
