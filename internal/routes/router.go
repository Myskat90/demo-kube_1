package routes

import (
	"demo-kube/internal/handlers"
	"demo-kube/internal/workload"
	"demo-kube/pkg/helpers/probs"
	"demo-kube/pkg/middleware"
	"github.com/d-kolpakov/logger"
	"github.com/go-chi/chi"
	"github.com/heptiolabs/healthcheck"
	"net/http"
)

func New(serviceName string, appVersion string, logger *logger.Logger, ic chan workload.Item) *http.Server {

	r := chi.NewRouter()
	m := middleware.New(logger)
	h := &handlers.Handler{L: logger, ServiceName: serviceName, AppVersion: appVersion, ItemChan: ic}

	// Health check
	hc := healthcheck.NewHandler()
	hc.AddReadinessCheck("readiness-check", func() error {
		return probs.GetReadinessErr()
	})
	hc.AddLivenessCheck("liveness-check", func() error {
		return probs.GetLivenessErr()
	})

	r.Use(m.RecoverMiddleware)
	r.Group(func(r chi.Router) {
		//System endpoints
		r.Group(func(r chi.Router) {
			r.Get("/", h.HomeRouteHandler)
			r.Get("/ready/", hc.ReadyEndpoint)
			r.Get("/live/", hc.LiveEndpoint)
		})

		// Application endpoints
		r.Group(func(r chi.Router) {
			r.Use(m.ContextRequestMiddleware, m.LogRequests)
			r.Get("/endpoint/", h.InternalEndpoint)
			//Public endpoints
			r.Get("/process/", h.AddProcess)
			r.Get("/error/", h.Error)
			/*r.Route("/public",func(r chi.Router) {
				r.Get("/process/", h.AddProcess)
				r.Get("/error/", h.Error)
			})*/
		})
	})
	server := &http.Server{Addr: ":8080", Handler: r}

	return server
}
