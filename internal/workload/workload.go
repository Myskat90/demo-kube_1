package workload

import (
	"context"
	"fmt"
	"github.com/fatih/color"
	"time"
)

type Workload struct {}

type Item struct {
	Id  int
}

func New() *Workload {
	w := &Workload{}
	return w
}

func (w *Workload) Start(ctx context.Context, ch chan Item) chan bool{
	exitChan := make(chan bool, 1)

	go func(ctx context.Context, ch chan Item, exitChan chan  bool) {
		for {
			select {
			case item := <-ch:
				w.SomeLoad(item.Id)
			case <-ctx.Done():
				fmt.Println("Context DONEEE")
				for {
					ctxTimeout, _ := context.WithTimeout(context.Background(), 1 * time.Second)
					select {
					case <-ctxTimeout.Done():
						exitChan <- true
						return
					case item := <-ch:
						w.SomeLoad(item.Id)
					}
				}
			}
		}
	}(ctx, ch, exitChan)

	return exitChan
}

func (w *Workload) SomeLoad(item int)  {
	color.New(color.FgYellow).Printf("Start process:  %d \n", item)
	time.Sleep(5 * time.Second)
	color.New(color.FgGreen).Printf("Process done:   %d \n", item)
}


