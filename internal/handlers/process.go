package handlers

import (
	"demo-kube/internal/workload"
	"demo-kube/pkg/helpers/probs"
	"demo-kube/pkg/helpers/response"
	"errors"
	"fmt"
	"net/http"
)

var Count int

func (h *Handler) AddProcess(w http.ResponseWriter, r *http.Request) {
	Count++
	fmt.Printf("Add process:    %d \n", Count)
	h.ItemChan <- workload.Item{Id: Count}
	response.JSON(w, http.StatusOK, "OK " + h.AppVersion)
}

func (h *Handler) Error(w http.ResponseWriter, r *http.Request) {
	probs.SetLivenessErr(errors.New("DB error: connection refuse"))

	response.JSON(w, http.StatusOK, "Some Error")
}



