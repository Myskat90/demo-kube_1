package handlers

import (
	"demo-kube/internal/workload"
	"demo-kube/pkg/helpers/response"
	"fmt"
	"net/http"
	"net/http/httputil"

	"github.com/d-kolpakov/logger"
)

type Handler struct {
	L           *logger.Logger
	AppVersion  string
	ServiceName string
	ItemChan    chan workload.Item
}

func (h *Handler) HomeRouteHandler(w http.ResponseWriter, r *http.Request) {
	byteDump, _ := httputil.DumpRequest(r, true)
	fmt.Fprintf(w,"Hello! This is %s. Version: %s", h.ServiceName, h.AppVersion)
	fmt.Fprintf(w, "\n" + string(byteDump))
}

func (h *Handler) InternalEndpoint(w http.ResponseWriter, r *http.Request) {
	response.JSON(w, http.StatusOK, "Internal endpoint.")
}

